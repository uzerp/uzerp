{** 
 *	(c) 2000-2012 uzERP LLP (support#uzerp.com). All rights reserved. 
 * 
 *	Released under GPLv3 license; see LICENSE. 
 **}
{* $Revision: 1.2 $ *}
<dl>
	<dt>Websites</dt>
		<dd><img src="/themes/default/graphics/companys_small.png" alt="Companies" />{link_to module="websites" controller="websites" value="all_websites"} &raquo;</dd>
</dl>
