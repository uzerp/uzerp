<?php
 
/** 
 *	(c) 2000-2012 uzERP LLP (support#uzerp.com). All rights reserved. 
 * 
 *	Released under GPLv3 license; see LICENSE. 
 **/
/**
 * Handles the displaying of the page called with no arguments
 */
class IndexController extends DashboardController {

	protected $version='$Revision: 1.5 $';
	
}

?>
