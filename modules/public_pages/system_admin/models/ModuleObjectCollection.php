<?php
 
/** 
 *	(c) 2000-2012 uzERP LLP (support#uzerp.com). All rights reserved. 
 * 
 *	Released under GPLv3 license; see LICENSE. 
 **/

class ModuleObjectCollection extends DataObjectCollection {

	function __construct($do='ModuleObject') {
		parent::__construct($do);

	}

}
?>