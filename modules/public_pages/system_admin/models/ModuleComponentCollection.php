<?php
 
/** 
 *	(c) 2000-2012 uzERP LLP (support#uzerp.com). All rights reserved. 
 * 
 *	Released under GPLv3 license; see LICENSE. 
 **/

class ModuleComponentCollection extends DataObjectCollection {

	function __construct($do='ModuleComponent') {
		parent::__construct($do);

	}

}
?>