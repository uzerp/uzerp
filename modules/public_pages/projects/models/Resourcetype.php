<?php
 
/** 
 *	(c) 2000-2012 uzERP LLP (support#uzerp.com). All rights reserved. 
 * 
 *	Released under GPLv3 license; see LICENSE. 
 **/
class Resourcetype extends DataObject {
	
	public function __construct($tablename='resource_types') {
		parent::__construct($tablename);
	}

}
?>