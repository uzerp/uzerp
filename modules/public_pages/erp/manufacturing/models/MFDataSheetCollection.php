<?php
 
/** 
 *	(c) 2000-2012 uzERP LLP (support#uzerp.com). All rights reserved. 
 * 
 *	Released under GPLv3 license; see LICENSE. 
 **/

class MFDataSheetCollection extends DataObjectCollection {

	function __construct($do='MFDataSheet') {
		parent::__construct($do);
	}

}
?>