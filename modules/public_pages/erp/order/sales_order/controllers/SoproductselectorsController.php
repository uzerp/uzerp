<?php

/** 
 *	(c) 2000-2012 uzERP LLP (support#uzerp.com). All rights reserved. 
 * 
 *	Released under GPLv3 license; see LICENSE. 
 **/

class SoproductselectorsController extends SelectorController
{

	protected $version = '$Revision: 1.11 $';
	
	public function __construct($module = null, $action = null)
	{
		parent::__construct($module, $action);
		
	}
	
}

// End of SoproductselectorsController
