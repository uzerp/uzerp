{** 
 *	(c) 2000-2012 uzERP LLP (support#uzerp.com). All rights reserved. 
 * 
 *	Released under GPLv3 license; see LICENSE. 
 **}
{* $Revision: $ *}
{content_wrapper}
	{include file="elements/datatable.tpl" collection=$reportparts}
{/content_wrapper}